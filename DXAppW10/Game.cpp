// Mandatory - all Windows dependencies - pre-compiled header
#include "pch.h"
#include "Game.h"
#include "Utils.h"

void MyGame::Initialize(CoreWindow^ pParentWnd) {

	this->gMainWindow = pParentWnd;

	DebugMessage(L"MyGame::Initialize ...\n");

	/* Fire up Direct3D - stuff gDevice and gDeviceContext */
	InitializeDirect3D();

	/* Obtain handlers to the GPU buffers */
	InitializeSwapChain();

	/* Set the initial render target - initially, we will set it to be a buckbuffer */
	InitializeRenderTarget();

	/* Loads initial Shaders and initializes all the pipeline */
	InitializePipeline();

	/* Set up viewport (How Direct3D interprets normalized coordinates) */
	InitializeViewport();

	/* Direct2D simple HUD */
	Initialize2DHud();
}

void MyGame::InitializeDirect3D() {
	/* Create device and deviceContext and convert them to DirectX 11.1 */
	// Temporary instances for converting the global gDevice and gDeviceContext
	ComPtr<ID3D11Device> dev11;
	ComPtr<ID3D11DeviceContext> contex11;

	// Add Direct2D support on Device
	UINT creationFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;

	// This will allow support for the lowest feature (last declared) on.
	D3D_FEATURE_LEVEL featureLevels[] =
	{
		D3D_FEATURE_LEVEL_11_1,
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
		D3D_FEATURE_LEVEL_9_3,
		D3D_FEATURE_LEVEL_9_2,
		D3D_FEATURE_LEVEL_9_1
	};

	// Only creates DX11 device, not 11.1
	D3D11CreateDevice(
		nullptr,					// pAdapter - specify video card or leave nullptr for DirectX to decide
		D3D_DRIVER_TYPE_HARDWARE,	// DriverType - use HW chip to process accelerated graphics instead of software
		nullptr,					// Software - if software was previously specified, we might point to a softwre engine
		creationFlags,				// Flags - UINT (FLAG1 | FLAG2 ... ) flags: https://msdn.microsoft.com/en-us/library/windows/desktop/ff476107%28v=vs.85%29.aspx?f=255&MSPPError=-2147217396
		featureLevels,					// D3D_FEATURE_LEVEL* pFeaturesLevels - video card features
		ARRAYSIZE(featureLevels),							// UINT FeatureLevel - how many features we indicated right before this parameter?
		D3D11_SDK_VERSION,			// SDKVersion - indicates which SDK version this app is meant for.
		&dev11,						// ID3D11Device** ppDevice - the device handler < it will get stuffed
		&gFeatureLevels,					// D3D_FEATURE_LEVEL* pFeatureLevel - a pointer with information about which features will be available for the programmer
		&contex11					// ID3D11DeviceContext** ppDeviceContext - the deviceContext handler < it will get stuffed properly
		);

	// Convert pointers from DirectX 11 (parameters) to DX 11.1 (callee) - we need this because no function can create DX11.1 pointers.
	dev11.As(&this->gDevice);
	contex11.As(&this->gDeviceContext);

	/* ---------------- DirectX Initialized !!! ---------------------- */
}

void MyGame::InitializeSwapChain() {
	/* 
		1. Obtain a pointer to a DXGI Factory COM object of our device and adapter - DXGI Factory can create DXGI Objects
			this will be given by the adapter identified by the D3D11CreateDivice done in the InitializeDirect3D function.	
			a. Obtain a IDXGI11Device1 ComPtr from gDevice (ID3D11Device1) using As
			b. Obtain the adapter (ComPtr<IDXGIAdapter>) by using the DXGI device
			c. Obtain a DXGI Factory COM Ptr from the adapter
	*/
	ComPtr<IDXGIFactory2> factory = GetDXGIFactory();

	/*
		2. Set up the description for out swap chain. We use struct DXGI_SWAP_CHAIN_DESC1.
			Here we set all the options such as Width and Height (to use other resolutions rather than default)
			BufferUsage (for what), BufferCount (how many)
			Format (how are pixels expressed), SwapEffect ( what swapping causes on render)
			SwapDesc.Count (how many Anti-Aliased passes per pixel)
	*/
	struct DXGI_SWAP_CHAIN_DESC1 swpDescriptor = GetSwapChainDescriptor();

	/*
		3. Fill up the swapChain pointer by using the DXGIFactory.CreateSwapChainForCoreWindow	
	*/
	CoreWindow^ mainWnd = CoreWindow::GetForCurrentThread();
	factory->CreateSwapChainForCoreWindow(
		gDevice.Get(),							// address of the device - IUnknown is ANY COM object in COM speak.
		reinterpret_cast<IUnknown*>(mainWnd),	// pass the main window.
		&swpDescriptor,							// address of the swap chain description
		nullptr,								// we can restrict output to a specific monitor by using this
		&gSwapchain								// pointer to our swap chain main COM ptr
	);
}

void MyGame::InitializeRenderTarget() {
	
	/* 1. CREATE Render Target */
	ComPtr<ID3D11Texture2D> backbuffer;
	// get the address of the backbuffer into &backbuffer
	gSwapchain->GetBuffer(0, __uuidof(ID3D11Texture2D), &gMainBackBuffer);
	
	/* 2. make the backbuffer to begRenderTarget for the device - hence the address */
	gDevice->CreateRenderTargetView(
		gMainBackBuffer.Get(),	// actual pointer to the interface 
		nullptr,				// render target description
		&gRenderTarget);		// the address of the actual render target to be created

}

void MyGame::InitializePipeline() {
	
	/* 1. Load Shader files */
	vector<byte> vectorShader = LoadShaderFile(ShaderVertexFile); // defined in Utils.cpp
	vector<byte> pixelShader = LoadShaderFile(PixelShaderFile);

	/* 2. Encapsulate Shaders into COM objects */
	gDevice->CreateVertexShader(vectorShader.data(), vectorShader.size(), nullptr, &gVertexShader);
	gDevice->CreatePixelShader(pixelShader.data(), pixelShader.size(), nullptr, &gPixelShader);

	/* 3. Set Shaders as active shaders */
	gDeviceContext->VSSetShader(gVertexShader.Get(), nullptr, 0);
	gDeviceContext->PSSetShader(gPixelShader.Get(), nullptr, 0);

	/* 4. Creates Input Elements to describe our Vertices (tell the GPU how to interpret them) */
	D3D11_INPUT_ELEMENT_DESC ied[] = {						// The properties of a vertex
	// parts of the struct we care about - note the 5th parameter is the offset of the byte number in the struct the value begins at
	// ied.SemanticName = "POSITION";						// What is this value used for
	// ied.Format = DXGI_FORMAT_R32G32B32_FLOAT;			// data type of the value we are passing
	// ied.InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;	// What is this element used for
		// POSITION property
		{"POSITION",0,DXGI_FORMAT_R32G32B32_FLOAT,0,0,D3D11_INPUT_PER_VERTEX_DATA,0},
		// COLOR property
		{ "COLOR",0,DXGI_FORMAT_R32G32B32_FLOAT,0,
			12,	// byte number in our structure the color property will start from
		D3D11_INPUT_PER_VERTEX_DATA,0 }
	};

	/* 5. Create the Input Layout */
	gDevice->CreateInputLayout(ied, ARRAYSIZE(ied), vectorShader.data(), vectorShader.size(), &gInputLayout);

	/* 6. Set the InputLayout for the pipeline */
	gDeviceContext->IASetInputLayout(gInputLayout.Get());
}

void MyGame::InitializeViewport() {
	
	D3D11_VIEWPORT viewPort = { 0 };

	/* 1. Set up view port properties */
	viewPort.TopLeftX = 0;
	viewPort.TopLeftY = 0;
	viewPort.Width = gMainWindow->Bounds.Width;
	viewPort.Height = gMainWindow->Bounds.Height;
	viewPort.MaxDepth = 1.0f;
	viewPort.MinDepth = 0.0f;
	/* 2. Set the view port in the gDeviceContext */
	gDeviceContext->RSSetViewports(
		1,				// # of viewports 
		&viewPort);		// a list (or single) viewport to use
}

void MyGame::Initialize2DHud() {

	/* Initialize Direct2D Hud */
	g2DHud.InitializeHud(gDevice, gSwapchain);

}

ComPtr<IDXGIFactory2> MyGame::GetDXGIFactory() {
	// 1. obtain a DXGI Device COM - Every ID3D11Device1 has a corresponding DXGIDevice1 object
	ComPtr<IDXGIDevice1> dxgiDevice;
	this->gDevice.As(&dxgiDevice);
	// 2. access the adapter - the virtual representation of our GPU
	ComPtr<IDXGIAdapter> adapter;
	dxgiDevice->GetAdapter(&adapter);
	// 3. access the DXGI Factory
	ComPtr<IDXGIFactory2> factory;
	adapter->GetParent(__uuidof(IDXGIFactory1), &factory); // __uuidof unique identifier for a the type of Interface we want to get, our adapter. We need version 2 for Windows 8 apps.
	return factory;
}

struct DXGI_SWAP_CHAIN_DESC1 MyGame::GetSwapChainDescriptor() {
	struct DXGI_SWAP_CHAIN_DESC1 swpDesc;
	swpDesc = { 0 };											// initialize all members to 0
	swpDesc.Width = 0;
	swpDesc.Height = 0;
	swpDesc.Scaling = DXGI_SCALING_NONE;
	swpDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;		// what are these buffers for?
	swpDesc.BufferCount = 2;									// use double buffering (draw and swap)
	swpDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;				// reg green blue and alpha
	swpDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL;		// what will flipping cause?
	swpDesc.SampleDesc.Count = 1;								// how to perform anti-aliased rendering - how many times AA per pixel
	return swpDesc;
}

void MyGame::Update() {
}

void MyGame::Render() {

	/* Set the render target as active */
	gDeviceContext->OMSetRenderTargets(
		1,								// number of render targets to set 
		gRenderTarget.GetAddressOf(),	// the address of a LIST of render targets
		nullptr);						// ??? I don't know

	/* Clear the RenderTarget and test it - aka make buffer blue */
	TestRenderTarget();

	/* Create a test triangle and put it into the gVertexBuffer */
	InitializeTestGraphics();

	/* Draw the buffer */
		// select the buffer we will draw
	UINT stride = sizeof(Vector3);
	UINT offset = 0;
	gDeviceContext->IASetVertexBuffers(
		0,								// slot number to draw
		1,								// number of buffers
		gVertexBuffer.GetAddressOf(),	// actual vertex buffer/s
		&stride,						// stride value for EACH buffer we are passing, in this case, just one. If many, it would be an array
		&offset);						// buffer's start offset
		// select the type of primitive we will draw
	gDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		// Draw
	D3D11_BUFFER_DESC bd = { 0 };
	gVertexBuffer->GetDesc(&bd);
	gDeviceContext->Draw(
		bd.ByteWidth / sizeof(Vector3),
		0);


	/* Draw 2D Hud */
	g2DHud.Render(this);

	/* Swap the chain by switching the front buffer with the back buffer */
	DXGI_PRESENT_PARAMETERS params = { 0 };
	gSwapchain->Present1(0,DXGI_PRESENT_DO_NOT_WAIT, &params); // <-- Free FPS
	// gSwapchain->Present(1, 0); // we will have the Windows App to manage our FPS (v-sync'ed) with buffers - caps at 60 FPS
}

void MyGame::TestRenderTarget() {
	float color[] = {0.0f, 0.2f, 0.4f, 1.0f };
	gDeviceContext->ClearRenderTargetView(gRenderTarget.Get(), color);
}

void MyGame::InitializeTestGraphics() {


	/* 1. create the vertex list */
	struct Vector3 testTriangle[] = {
		{ 0.0f, 0.5f, 0.0f },
		{ 0.45f, -0.5f, 0.0f },
		{ -0.45f, -0.5f, 0.0f }
	};

	/* 2. Create the ID3D11Buffer COM ( vertex-buffer) - VERY IMPORTANT */
		// properties of the buffer
	D3D11_BUFFER_DESC bd = { 0 };
	bd.ByteWidth = sizeof(struct Vector3) * ARRAYSIZE(testTriangle);	// set its size
		// what kind of buffer will this be
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;							// set it as a vertex buffer
		// this isa description of the data we are pushing into the GPU vis this buffer
	D3D11_SUBRESOURCE_DATA sd = { testTriangle, 0 ,0 };

		// we have the device to create our buffer
	gDevice->CreateBuffer(&bd, &sd, &gVertexBuffer);
}

bool MyGame::Tick() {
	return gTicker.Tick();
}

void MyGame::UpdateBackbufferSize(UINT pWidth, UINT pHeight) {
	
	/* 1. Clear render targets from device context */
	ID3D11RenderTargetView* nullViews[] = { nullptr };
	gDeviceContext->OMSetRenderTargets(ARRAYSIZE(nullViews), nullViews, nullptr);
	
	/* 2. Release Rendering Target from Direct3D and the Hud (Direct2D) */
	gRenderTarget.Reset();
	gMainBackBuffer.Reset();
		// release Direct2D resources
	g2DHud.ResetHud();

	/* 3. Resize buffer */
	HRESULT hr = gSwapchain->ResizeBuffers(
		0,
		0,
		0,
		DXGI_FORMAT_UNKNOWN,
		0
		);
	char buf[256];
	sprintf_s(buf, "Buffer Resize Failed with code: %10x\n", (int)hr);
	if (FAILED(hr)) OutputDebugStringA(buf);
	/* 4. Reset the buffer as target view */
	gSwapchain->GetBuffer(0, __uuidof(ID3D11Texture2D), &gMainBackBuffer);
	gDevice->CreateRenderTargetView(gMainBackBuffer.Get(), nullptr, &gRenderTarget);

	/* 5. Set the new render target and reinitialize Hud */
	gDeviceContext->OMSetRenderTargets(1, gRenderTarget.GetAddressOf(), nullptr);
	g2DHud.InitializeHud(gDevice, gSwapchain);

	/* 6. Reset view port */
	D3D11_VIEWPORT vp = { 0 };
	vp.Width = pWidth;
	vp.Height = pHeight;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	gDeviceContext->RSSetViewports(1, &vp);
	
}