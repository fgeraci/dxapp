#pragma once

// Mandatory - all Windows dependencies - pre-compiled header
#include "pch.h"
#include "Ticker.h"

/* FORWARD DECLARING CLASSES FOR COMPILATION */

using namespace Microsoft::WRL;
using namespace Windows::UI::Core;
using namespace DirectX;

/* FORWARD DECLARATIONS */
class Hud;

ref class MyGame sealed {

private:

	/* Parent core window */

	CoreWindow^ gMainWindow;

	/* Setting up Direct3D COM objects */

		// virtual video card representation - access memory, create other COM D3D objects in there
	ComPtr<ID3D11Device1> gDevice;

		// control of rendering sequence and pipeline from models to a 2D screen-ready image.
	ComPtr<ID3D11DeviceContext1> gDeviceContext;

		// pointers to the GPUs drawing buffers
	ComPtr<IDXGISwapChain1> gSwapchain;

		// Location in memory for us to render into < we paint here
	ComPtr<ID3D11RenderTargetView> gRenderTarget;

		// Reference to main back buffer when creating rendering target
	ComPtr<ID3D11Texture2D> gMainBackBuffer;

		// We wirte to this buffer for thentransferring to GPU's memory - all vertices
	ComPtr<ID3D11Buffer> gVertexBuffer;

		// This represent the information we are pushing into the GPU. It let's the GPU understand what is getting
	ComPtr<ID3D11InputLayout> gInputLayout;

		// THis will be returned upon a device is created while initialize said device
	D3D_FEATURE_LEVEL gFeatureLevels;

		// Holds Direct2D renderer for a simple Hud
	Hud g2DHud;

		// Frame rate controller
	Ticker gTicker;

		// Shaders files and COMs
	const std::string ShaderVertexFile	= "VertexShader.cso";
	const std::string PixelShaderFile	= "PixelShader.cso";
	ComPtr<ID3D11VertexShader>	gVertexShader;
	ComPtr<ID3D11PixelShader>	gPixelShader;

	/* ------------------------------- */

	/* Set Up - In Order */

	void InitializeDirect3D();

	void InitializeSwapChain();

	void InitializeRenderTarget();

	void InitializePipeline();

	void InitializeViewport();

	void Initialize2DHud();

	ComPtr<IDXGIFactory2> GetDXGIFactory();
	struct DXGI_SWAP_CHAIN_DESC1 GetSwapChainDescriptor();
	// void SetSwapChainDescriptor(struct DXGI_SWAP_CHAIN_DESC1*);

	/* Clear the buffer and draw blue into it */
	void TestRenderTarget();

	/* Just draw a simple triangle */
	void InitializeTestGraphics();

	/* ------- */

public:

	/* Prepare Direct3D for use */
	void Initialize(CoreWindow^ pParentWindow);
		// we will break the Direct3D initialization, SwapChain, etc.. into different functions in Game.cpp

	/* Timely update our game's logic */
	void Update();
	
	/* Draw single frame of GFXs */
	void Render();

	/* Control */
	bool Tick();

	/* Updates back buffer size on resize */
	void UpdateBackbufferSize(UINT pWidth, UINT pHeight);

	/* PROPERTIES */
	property int CurrentFPS {
		int get() {
			return gTicker.GetCurrentFPS();
		}
	}

};