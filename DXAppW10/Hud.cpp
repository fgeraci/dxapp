#include "pch.h"
#include "Hud.h"

void Hud::InitializeHud(ComPtr<ID3D11Device1> pDevice,
	ComPtr<IDXGISwapChain1> pSwapchain) {

	ID2D1Factory1* d2dFactory;

	/* 1. Create the Direct2D factory */
	HRESULT r = D2D1CreateFactory(
		D2D1_FACTORY_TYPE_SINGLE_THREADED,
		&d2dFactory
		);

	/* 2. Create de Device and DeviceContext */
	ComPtr<IDXGIDevice1> dxgiDevice;
	pDevice.As(&dxgiDevice);

	if (SUCCEEDED(r)) {
		d2dFactory->CreateDevice(dxgiDevice.Get(), &gDevice);
		gDevice->CreateDeviceContext(D2D1_DEVICE_CONTEXT_OPTIONS_NONE, &gDeviceContext);

		/* 3. Create a DXGI Bitmap which then will become the rendering target - ALWAYS USE D2D1::BitmapProperties1(... */
		
		FLOAT dpiX, dpiY;

		d2dFactory->GetDesktopDpi(&dpiX, &dpiY);

		D2D1_BITMAP_PROPERTIES1 bitmapProperties =
			D2D1::BitmapProperties1(
				D2D1_BITMAP_OPTIONS_TARGET | D2D1_BITMAP_OPTIONS_CANNOT_DRAW,
				D2D1::PixelFormat(DXGI_FORMAT_B8G8R8A8_UNORM, D2D1_ALPHA_MODE_IGNORE),
				dpiX,
				dpiY
				);

		pSwapchain->GetBuffer(0, IID_PPV_ARGS(&gBackBuffer)); // IID_PP ... __uuiof(IDXGISurface) MACRO

		gDeviceContext->CreateBitmapFromDxgiSurface(gBackBuffer.Get(), &bitmapProperties, &gTargetBitmap);

		/* 4. effectively set the rendering target of D2D context to be the D2D1Bitmap1 */
		gDeviceContext->SetTarget(gTargetBitmap.Get());

	}

	/* Init elements */
		
		// a. Create Write Factory
	DWriteCreateFactory (
		DWRITE_FACTORY_TYPE_SHARED,
		__uuidof(IDWriteFactory),
		&gWriteFactory
		);

		// b. Create Text Format
	gWriteFactory->CreateTextFormat(
		L"Calibri", 
		nullptr, 
		DWRITE_FONT_WEIGHT_LIGHT, 
		DWRITE_FONT_STYLE_NORMAL, 
		DWRITE_FONT_STRETCH_NORMAL, 
		32, 
		L"en-us", 
		&gTextFormat);
		
		// c. Create Brush
	gDeviceContext->CreateSolidColorBrush(
		D2D1::ColorF(D2D1::ColorF::GreenYellow), 
		&gBrush);


}

void Hud::Render(MyGame^ pGame) {
	/* Render Hud here, using the game's information */
	gDeviceContext->BeginDraw();
	
	static const int bufferLength = 256;
	static char16 wsbuffer[bufferLength];
	int length = swprintf_s(wsbuffer,bufferLength,L"Demo App\nFPS:\t%4d\n", pGame->CurrentFPS);

	gDeviceContext->DrawText(
		wsbuffer,
		length,
		gTextFormat,
		&gTitleRect,
		gBrush.Get()
		);

	gDeviceContext->EndDraw();
}

void Hud::ResetHud() {
	gDeviceContext->SetTarget(nullptr);
	gTargetBitmap.Reset();
	gBackBuffer.Reset();
}