#pragma once

#include "pch.h"

using namespace Microsoft::WRL;
using namespace Windows::UI::Core;
using namespace DirectX;

/* FORWARD DECLARATION */
ref class MyGame;

class Hud {
private:
	
	ID2D1Device* gDevice;
	ID2D1DeviceContext* gDeviceContext;
	ComPtr<ID2D1Bitmap1> gTargetBitmap;
	ComPtr<IDXGISurface> gBackBuffer;
	ComPtr<ID2D1SolidColorBrush> gBrush;

	/* Text */
	ComPtr<IDWriteTextFormat> gtextFormatTitle;
	ComPtr<IDWriteFactory> gWriteFactory;

	IDWriteTextFormat* gTextFormat;

	/* Rectangles */
	const D2D1_RECT_F gTitleRect = D2D1::RectF(10.0f, 5.0f, 250.0f, 100.0f);

public:
	void InitializeHud(ComPtr<ID3D11Device1>, ComPtr<IDXGISwapChain1>);
	void Render(MyGame^);
	void ResetHud();
};
