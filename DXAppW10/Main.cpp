// Mandatory - all Windows dependencies - pre-compiled header
#include "pch.h"
#include "Game.h"

// Add required namespaces to simplify code - manually
using namespace Windows::ApplicationModel;
using namespace Windows::ApplicationModel::Core;
using namespace Windows::ApplicationModel::Activation;
using namespace Windows::UI::Core;
using namespace Windows::UI::Popups;
using namespace Windows::System;
using namespace Windows::Foundation;
using namespace Windows::Graphics::Display;
using namespace Platform;


/* Summary 

	Considerations of Universal Windows Platform apps (UWP):
		* Project must be set as Debug -> x86
		* The provided PixelShader must be marked (Right click > Properties > Shader Type) as Pixel Shader
		* Use ProcessAllIfPresent for non-blocking input
		* FPS control: 
				Present(1,0) <- from the swap chain is always locked at the monitor's refrsh rate, so we can do 60 FPS all the time
				Present1(0,DXGI_PRESENT_DO_NOT_WAIT, &params) <- unlocks FPS so we can use our own ticket to refresh
		* QueryPerformanceCounter is the way to go for accurate time measurements / GetTickCount64() doesn't work
		* We can debug to console using OutputDebugStringA(<string>)

	SET UP APP:
	----------

	1) Out main application will be an instance of IFrameworkView (Interface)
		* this one will be used for the app's lifecycle with --> Initialize() - 
																--> SetWindows() 
																	--> Load() - Load/init GFXs, Sounds, DirectX, etc..
																		--> OnActivated() - activates the window app's window 
																			--> Run() - app's main loop
																				--> Uninitialize()
	2) This instance, will be created and return by another class, which implements the IFrameworkViewSource interface
		* this only needs to implement the virtual method IFrameworkView^ CreateView()
	3) main simply calls CoreApplication::Run(...) which will need the above described elements.
	4) We will also add gElapsedTime and gFrameRateTarget to manage the refresh rate of the game loop as private members

	WINDOW / IO EVENTS HANDLERS:
	---------------
	1) Set the window to listen to events if present (this will keep it open) in the Run() while(!windowClosed) { coreWnd->Dispatcher->ProcessEvents( <here> ) ... }
 		a. ProcessUntilQuit < blocking infinite loop which is good for 100% event driven apps. All logic happnes in the hanlders. (NOT GOOD FOR GAMES)
		b. ProcessAllIfPresent < it returns as soon ALL events have been handling, allowing us to continue with our game loop.
	2) Define events handlers in our App:IFrameworkView::SetWindow() function class - Window, Pointer, Key etc...
		Create a handler function in IFrameworkView
			Register this function in SetWindow(...) - either IO / Window event
				wnd->Listener += TypedEventHandler<CoreWindow^,SomeEventArgs^>(this,&App::MyListener)
				wnd->Closed += ref new ...

	APP EVENTS:
	----------
	Managed by the OS via signaling: Not Running > Activated > Running <> Suspended > Terminated 
	1) Define handlers just like I/O - inside the App : IFrameworkView instance
	2) Register the handlers in the App::Initialize() using CoreApplication::<event>

	SET UP APPLICATION LOOP:
	-----------------------

	All the action - during runtime - will happen in App::Run() - e.g. Initialize() - set up Direct3D and the SwapChain -, Update() - game logic -, Render() - draw it - 

	1) Create a flag for checking if the window was closed in App:IFrameworkView (our main window instance/hanlder - not to confuse it with our game instance)
		while(!windowClosed) { ... }
	2) More to come ... (actual game loop - frame rendering - implementation

	CREATE BASIC GAME FILES:
	-----------------------

	1) Create a header file (for example Game.h) which will use namespaces Microsoft::WRL, DirectX and Windows::UI::Core
			class MyGame {
			public:
				void Initialize();	// beginning of Game
				void Update();		// Timing and update game logic
				void Render();		// draw GFXs
			}
	2) Create the .cpp file for Game.h which #include's "Game.h" and implements Initialize(), Update() ... etc
	3) Add an instance of MyGame to our App:IFrameworkView (header in file, instance and Update() and Render() in App's loop).
		Run() {
			initialize game
			while(!windowClosed) {
				read input - ProcessAllIfPresent
				game.Update
				game.Render
			}
		}
	
	SETTING UP DIRECT3D:
	-------------------

	Direct3D is a COM made out of some other COMs, for this we will need to:
		a. Create a "Smart Pointer" (a class in charge of handling certain aspects of the COM)
			ComPtr<IWeNeed> comPtr;
		b. Create the COM object with a function WE implement, in this case, CreateObject(ptr*).
			CreateObject(&comPtr);
	1) Create the Device and DeviceContext smart pointers in Game.h
		Device - <ID3DDevice1> actual virtual CPU - here we access memory to create more COM objects
		DeviceContext - <ID3DDeviceContext1> rendering sequence and pipeline
		Setting UINT creationFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT in the creationFlags parameter of D3D11CreateDevice allows support for Direct2D
		Set up the Feature levels for backward support
	2) in MyGame::Initialize, fire off D3D!
		a. Create device and deviceContext and convert them from DirectX11 to 11.1 / 2 using .As()

	SETTING UP THE BUFFERS (Swap Chain):
	----------------------

	We will setup the Swapping Chain to avoid "tearing" (image redrawn while monitor refreshes) - only draw after a image is full.
	Multi buffer swapping occurs everytime a new frame is pass to the screen.
		(Third Buffer) <- Front Buffer <- Back Buffer <- Third Buffer <- (Front Buffer)
	1) Add the swapchain (IDXGISwapChain1) ComPtr to Game.h - we will draw into these buffers
	2) Initialize the Swap Chain in Game.cpp - by the end of this, the device and the window will have the buffers to their disposal
		a. Obtain a pointer for DXGI Factory - the one which created our device AND adapter
		b. Create and fill the swap chain descriptor (struct DXGI_SWAP_CHAIN_DESC1)
			- we can customize how many buffers, their sizes, etc ...
			- this descriptor can be further customized by using Flags, for future reference
	3) Use the obtained DXGI Factory to call CreateSwapChainForCoreWindow(device,window,descriptor,restriction,swapchainCOMptr)
	
	Done, moving forward, the buffers can be swapped: Render() { gSwapchain->Present(1,0); } // this flips the back and the front buffer

	SETTING UP THE RENDER TARGET
	----------------------------

	Now that we have Direct3D and our Swapchain are up, we can start rendering frames. For which we must:

	1) Set up the Render Target (our buffers, although this could change eventually) - this will happen in InitializeRenderTarget
		a. CreateRenderTarget

	2) Now, we can set it as the active one EACH FRAME - in the Render() function - we use the device context for that
		We do this by using the gDeviceContext->OMSetREnderTargets - we could potentially have multiple rendering targets
	
	As I said before, we will use the gDeviceContext to set the target as active and draw to it (the back buffer), once we are ready, we
	simply use the swapchain to place the right buffer in place for each frame

	INITIALIZE THE PIPELINE
	-----------------------

	Tell the GPU how to read these vertices < A.K.A Program the renderign pipeline using Shaders
	INPUT >> Vertices >> Processing (Shaders) >> OUTPUT >> Image
	We can program Shaders (to control steps of the rendering pipeline) or load them, or both.
	
	1. Load Shaders:
		1. Create a vertex and pixel shaders files - this creates a .hlsl which compiles into a .cso
			Add > New Item ...  > HLSL (high level shader language) > Vertex and Pixel Shaders
		2. Load them from a .cso (Compiled Shader Object) file
			When built, hlsl compile into .cso (compiled shader object) - we need to load them - Utils.LoadShaderFile(...)
		3. Encapsulate them into Shader objects - ComPtr<ID3D11[Vertex|Pixel]Shader>
			We use the Device->Create[Vertex|Pixel]Shader to populate the COM ptrs to the ID3D11[type]Shaders
		4. Set them as active Shaders
			We set them into the device context (gDeviceContext->VSSetActive(<shader and params>))

	Vertex Shaders will handle the 3D interpretation of our vertices
		If there is 1 polygon (triangle), the Vertex shader will be called 3 times (1 per vertex) each frame
	Pixel Shaders will handler the coloring of the polygons we draw
		This run once per every visible pixel per frame

	2. Tell the GPU how to translate those vertices into a flat image
		a. create the Input Elements to define our Vertex parts
			D3D11_INPUT_ELEMENT_DESC will hold the properties of our Vector3 struct elements.
	
	3. Create an InputLayout object
			Globally delcared on Game.h - we use the gDevice->CreateInputLayout
	
	4. Set the created gInputLayour object as the Input Layout for the context
		gDeviceContext->IASetInputLayout(gInputLayout.Get())

	INITIALIZE VIEWPORT
	-------------------

	We set up how Direct3D places normalized coordinates on the backbuffer, through the gDeviceContext->RSSetViewports(...). A regular layout could be:
	
	For example (x,y)

	   -1,-1			1,-1

			   0,0			

	   -1, 1			1, 1				

	We will do this in MyGame::InitializeViewport()

	1) Create a D3D11_VIEWPORT struct
		a. Set up TopLeftY, TopLeftX, Width (gMainWindow->Bounds.Width) and Height the same way

	2) Set the view port (or view ports - see function for details -) in the gDeviceContext->SRSetViewports(...)

	DRAWING A TRIANGLE - basic 3D practical concepts
	------------------
	DirectX uses input-layout < we select which data to send from each struct, in order to optimizie and increase through-put

	Of course we will do in the main Render() - calling a test function InitializeTestGraphics - function within Game.cpp

	1. Create three vertices to make a triangle.
		a. we created (self-defined) a Vector3 struct, then added a Initialize[Test]Graphics method to MyGame (Game.cpp) for creating a 3 vertices array of struct Vector3

	2. Putting vertices into Vertex buffer (gVertexBuffer in Game.h) for video memory.
		a. For this, we use Direct3D COM memory vertex-buffer object which is our way to move Main Memory into GPU Memory
			Game.h > add ComPtr<ID3D11Buffer> gVertexBuffer member
		b. In InitializeGraphics (InitializeTestGraphics for testing), set up gVertexBuffer < we write to this buffer and then indicate Direct3D to write it into the GPU's memory
			Create a D3D11_BUFFER_DESC, D3D11_SUBRESOURCE_DATA to specify the buffer's type
		c. Use the device to create the buffer gDevice->CreateBuffer(...)

	3. Set the vertices and their type we want to draw (how to handle the buffer we created). - using the gDeviceContext
		a. Set which Vertex Buffer we will use
			IASetVertexBuffer(...)
		b. Set the type of primitive we will use - Point list, Line list, Line strip, Triangle list, Triangle strip
			IASetPrimitiveTopology(...)

			* Primitives

			Point 		- single point

			Line List	- 1 line for every 2 points exclusively - there will be N / 2 lines per list

				*	  *
			   /     /
			  /     /
			 *     *

			Line Strips - 1 line for every point to point - there will be N - 1 lines per list

				*	  *
			   / \   /
			  /   \ /
			 *     *

			Triangle List - there will be N / 3 triangles per list < this draws 1 triangle per 3 points

			Traingle Strips - There will be N - 2 trignles per list	

		c. Finally, draw the shape
			gDeviceContext->Draw(how many vertices, vertex offset)

	GAME LOOP TICKER
	----------------

	I created a Ticker class which will be used by Game. Game will implement a Tick() method which return bool TRUE/FALSE if it
	is indeed tick time (Update/Render). Game.Tick() { this->gTicker.Tick() } so the actual implementation of Tick() is from Game's
	member ticker. Tick will simple calculate deltaTime from last tick and return TRUE/FALSE depending on whether delta >= (1000/TargetFPS) is achieved.
	we will be measuring in milliseconds.

	Additionally, Ticker has a GetCurrentFPS() method which returns current FPS (after second 1, of course)

	Ticker implementation is on Ticker.h/cpp

	Details:

	For high accuracy, I am using QueryPerformanceCounter / QueryPerformanceFrequency. GetTciksCount64() is not accurate enough, IT WILL NOT WORK.

	Basically class Ticker has a LARGE_INTEGER gFrequency member which is initialized on creation )constructor)
	utilizing QueryPerformanceFrequency.
	Along with the frequency, there is a LARGE_INTEGER gStartTime which will also be initialized on the constructor
	by using QueryPerformanceCounter.
	There, every time we need the current time since the program began, we simply call GetCurrentTime which will return
	(1000LL * (Now - gStartTime) / gFrequency) in milliseconds. The resolution is high enough to accurately cap 30/60/N FPSs

	DRAWING THE HUD - Using Direct2D with a Direct3D device
	---------------
	
	In Hud.h, Hud.cpp

	- NOTE: We will use the device and swap chain we created for Direct3D11.
		The Device MUST be compatible for Direct2D. This is achieve by adding the creationFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT
		parameter to the D3D11CreateDevice method

	After successfully initialized and tested direct 3d, we can add a bit to the screen. We will use Direct2D to draw the some HUD like information.
	1) Add the header files to the projects 'pch.h' - d2d1.h and d2d1_1.h
	2) Create the Device and set Render Target for the the D2D Context
		Within InitializeHud:
		a. Create a ID2D1Factory1 factory
		b. Use the original D3D11Device1 to create a dxgiDevice
		c. Create a D2D11Device1 by using the factory
		d. Create a DeviceContext
		e. Obtain the swap chain (already existing one) buffer and make it into a IDXGISurface buffer
		f. Use the DeviceContext (Direct2D's) and set the IDXGISurface buffer as the render target
	3) Once D2D has been initialized, set up the elements to draw to screen
		a. A rectangle for each section on which we will draw
		b. a Text format and brush
	4) For drawing, in a Render() method, we will
		a. use gDeviceContext->BeginDraw()
		b. DrawText / ... whatever you would like
		c. gDeviceContext->EndDraw()

		DONE!

*/

/*
	This class implements an interface for the principal instance of our application.
*/
ref class App sealed : public IFrameworkView
{
private:

	MyGame gGame;
	CoreWindow^ gCoreWnd;
	bool gWndClosed = false;

protected:

	property bool WindowClosed {
		bool get() {
			return this->gWndClosed;
		}
	}

	property CoreWindow^ MainWindow {
		CoreWindow^ get() {
			return this->gCoreWnd;
		}
	}

public:
	/* --- App LIFE-CYCLE functions called by Windows --- */
		
	/* 1. <-- AppView gives us access to low level properties and notifications - register Window and Applications events */
	virtual void Initialize(CoreApplicationView^ AppView) { 
		
		/* Register Window's event handlers */
		AppView->Activated += ref new TypedEventHandler		// pass OnActivate function address as the handler for the window's creation
															// to the Activated event handler - match its parameters with the generic <T,Ts>
			<CoreApplicationView^, IActivatedEventArgs^>(this, &App::OnActivated);
		
		/* Application's state - CoreApplication::<event> handlers */
		CoreApplication::Suspending += ref new EventHandler
			<SuspendingEventArgs^>(this, &App::OnSuspending);
		CoreApplication::Resuming += ref new EventHandler
			<Object^>(this, &App::OnResuming);
		CoreApplication::Exiting += ref new EventHandler
			<Object^>(this, &App::OnExiting);

		DebugMessage(L"App::Initialize(...)\n");
	}

	/* 2. <-- here we can set up I/O and Windows listeners called after Initialize() */
	virtual void SetWindow(CoreWindow^ wnd) { 
		DebugMessage(L"App::SetWindow(...)\n");

		/* Set up Properties */

		this->gCoreWnd = wnd;
		
		/* Windows event handlers */

		wnd->Closed += ref new TypedEventHandler
			<CoreWindow^, CoreWindowEventArgs^>(this, &App::OnWindowClosed);

		wnd->SizeChanged += ref new TypedEventHandler
			<CoreWindow^, WindowSizeChangedEventArgs^>(this,&App::OnWindowResized);

		/* IO events handlers */

		wnd->PointerPressed += ref new TypedEventHandler
			<CoreWindow^, PointerEventArgs^>(this, &App::PointerPressed);
		wnd->KeyDown += ref new TypedEventHandler
			<CoreWindow^, KeyEventArgs^>(this, &App::KeyPressed);
		wnd->KeyUp += ref new TypedEventHandler
			<CoreWindow^, KeyEventArgs^>(this, &App::KeyReleased);
		wnd->PointerReleased += ref new TypedEventHandler
			<CoreWindow^, PointerEventArgs^>(this, &App::PointerReleased);
		wnd->PointerWheelChanged += ref new TypedEventHandler
			<CoreWindow^, PointerEventArgs^>(this, &App::PointerWheelHandler);
	}

	/* 3. <-- Good place to load GFXs and Sounds - general resources */
	virtual void Load(String^ EntryPoint) { DebugMessage(L"App::Load(...)\n"); }
	
	/* 4. <-- We effectively create a window here and set it activated */
	void OnActivated(CoreApplicationView^ CoreAppView, IActivatedEventArgs^ Args) {
		CoreWindow^ coreWnd = CoreWindow::GetForCurrentThread();
		coreWnd->Activate();
		DebugMessage(L"App::OnActivated(...)\n");
	}

	/* 5. <-- This will be our main app's loop - I put it outside the class definition, under main */
	virtual void Run();
	
	/* 6. <-- Die time, clean everything loaded in Load() */
	virtual void Uninitialize() { DebugMessage(L"App::Uninitialize(...)\n"); }

	/* Events Handlers - definitions */

		/* Pointer */
	void PointerPressed(CoreWindow^ pWnd, PointerEventArgs^ args) {
		// args->CurrentPoint will provide a lot of info: X,Y, PointerID
	}
	void PointerReleased(CoreWindow^ pWnd, PointerEventArgs^ args) {}
	void PointerWheelHandler(CoreWindow^ pWnd, PointerEventArgs^ args) {
		int wheelChange = args->CurrentPoint->Properties->MouseWheelDelta;
	}
	
		/* Keys */
	void KeyPressed(CoreWindow^ pWnd, KeyEventArgs^ args) {
		switch (args->VirtualKey) {
		case VirtualKey::Escape:
			MessageDialog dialog(L"Do want to exit?");
			dialog.ShowAsync();
		}
	}
	void KeyReleased(CoreWindow^ pWnd, KeyEventArgs^ args) { }

		/* Application - top level app's state handlers */
	void OnSuspending(Object^ pSender, SuspendingEventArgs^ args) {
		DebugMessage(L"App::Suspending ... \n");
	}
	void OnResuming(Object^ pSender, Object^ args) {
		DebugMessage(L"App::Resuming ... \n");
	}
	void OnExiting(Object^ pSender, Object^ args) {
		DebugMessage(L"App::Exiting ... \n");
	}

		/* Main window handlers */
	void OnWindowClosed(CoreWindow^ pWnd, CoreWindowEventArgs^ args) {
		DebugMessage(L"Closing main window ...");
		this->gWndClosed = true;
	}

	void OnWindowResized(CoreWindow^ pWnd, WindowSizeChangedEventArgs^ args) {
		UINT width = args->Size.Width;
		UINT height = args->Size.Height;
		gGame.UpdateBackbufferSize(width, height);
	}
	
	/* ----------------------------- */
	
};

/*
	This class will create THE instance of our application
*/
ref class AppSource sealed : IFrameworkViewSource {
public:
	virtual IFrameworkView^ CreateView() {
		return ref new App();
	}
};

/* MULTI-THREADED-APARTMENT THREAD - The model DirectX uses to handle Threads in the app 
	This tells windows to run our app in multi-threated-apartment mode. */
[MTAThread]

/* THE STARTING POINT */
int main(Array<String^>^) { /* <-- Array<String^>^ is simply handlers to and array of strings in WinRT language */

	/* Invokes Run -> Invokes CreateView on IFrameworkViewSource to 
		retrieve the important IFrameworkView instance (our application) */
	CoreApplication::Run(ref new AppSource());
	return 0;
}

void App::Run() {

	// this is our application main loop
	DebugMessage(L"App::Run(...)\n");

	//Initialize Game
	gGame.Initialize(gCoreWnd);
	
	while (!(this->WindowClosed)) {

		/* Different types of Application's events processing
		ProcessUntilQuit <- is a blocking infinite loop until the Closed event is triggered. This is fully event drive rather than giving control to loop.
		alternative > coreWnd->Dispatcher->ProcessEvents(CoreProcessEventsOption::ProcessUntilQuit); << infinite loop, not good.
		We want to be in control of the processing loop, so instead, we will use ProcessAllIfPresent
		*/
		this->MainWindow->Dispatcher->ProcessEvents(CoreProcessEventsOption::ProcessAllIfPresent);

		if(gGame.Tick()) {
			gGame.Update();
			gGame.Render();
		}
		
	}

	DebugMessage(L"App::Run() terminating ...\n");
}

	/* 
	
	NOTES 

	* ref classes are similar to regular classes but:
		-	only "sealed (cannot be inherited by other classes)" ref classes can have a public constructor
		-	they are automatically garbage collected when nothing uses the "hat" pointer
			after the i is nonstance longer in scope
		-	it cannot contain public variable members, but it can only be accessed via
			properties such as:

			private:
				int _number;
			public:
				property int Number
				{
					int get() { return _number; }
					void set(int val) { _number = val; }
				}
		-	example

				ref class box { ... };
				... 
					box^ myBox = ref new box;
					myBox->Number = 5;
					// delete myBox on exit
				}
				...
	
	* properties are just like c# etc
		public:
			property bool Something {
				bool get() {
					return true;
				}
				void set(<arg>) {
					this->Bla = <args>;
				}
			}


	* ComPtr<MyInterface> is used to manage COM objects

	*/

