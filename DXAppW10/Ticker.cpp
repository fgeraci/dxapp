#pragma once

#include "pch.h"
#include "Ticker.h"

int Ticker::GetCurrentFPS() {
	return this->gCurrentFrameRate;
}

double Ticker::GetCurrentTime() const {
	LARGE_INTEGER EndTime;
	QueryPerformanceCounter(&EndTime);
	return 1000LL * ((EndTime.QuadPart - gStartTime.QuadPart) / gFrequency);
}

bool Ticker::Tick() {

	double currentTick = GetCurrentTime();
	double deltaTicks = (currentTick - gLastTick);
	bool tick = false;

	// determine whether it is tick time
	if (deltaTicks >= gUpdateTime) {
		gLastTick = currentTick;
		gTicksCount++;
		// char buf[256];
		// sprintf_s(buf, "Current delta: %30d\n", deltaTicks);
		// OutputDebugStringA(buf);
		tick = true;
	}

	// this metric must happen regardless of actual true Ticks
	if (currentTick - gSecondTime >= 1000) {
		gCurrentFrameRate = gTicksCount;
		gSecondTime = currentTick;
		// OutputDebugStringW(L"Second is up\n");
		gTicksCount = 0;
	}

	return tick;
}

int Ticker::GetFPS() { return 0; }

void Ticker::SetTargetFrameRate(int target) {
	this->gFrameRate = target;
}