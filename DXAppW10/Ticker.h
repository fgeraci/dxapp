#pragma once

#include "pch.h"

class Ticker {

private:
	
	int gFrameRate;
	long gTicksCount;
	long gLastTick;
	float gSecondTime;
	double gUpdateTime;
	
	double gFrequency;
	LARGE_INTEGER gStartTime;

	int gCurrentFrameRate;

	double GetCurrentTime() const;

public:

	// 30 FPS default target
	Ticker() : gFrameRate(60), gSecondTime(0), gTicksCount(0), gCurrentFrameRate(0), gUpdateTime(1000.0/(double)gFrameRate)  { 
		LARGE_INTEGER PerfCounter;
		QueryPerformanceFrequency(&PerfCounter);
		gFrequency = (double) PerfCounter.QuadPart;
		QueryPerformanceCounter(&gStartTime);
		gLastTick = GetCurrentTime();
	}
	
	Ticker(int frameRate) : Ticker() {
		gFrameRate = frameRate;
	}

	// main function which will lock / unlock
	bool Tick();
	int GetFPS();
	void SetTargetFrameRate(int);

	/* Accessors */
	int GetCurrentFPS();
};