#include "pch.h"
#include "Utils.h"


const float Epsilon = 0.001f;

vector<byte> LoadShaderFile(std::string pFileName) {
	
	// create a generic Array object
	vector<byte>shaderData;

	// Instatiate a VertexFile object by using its parameterized constructor to open and read shader
	std::ifstream VertexFile(pFileName, std::ios::in | std::ios::binary | std::ios::ate);

	if (VertexFile.is_open()) {
		// get file's length
		int length = (int) VertexFile.tellg();

		// populate data array
		vector<byte> shaderData(length);
		VertexFile.seekg(0, std::ios::beg);
		VertexFile.read(reinterpret_cast<char*>(shaderData.data()), length);
		VertexFile.close();
		return shaderData;
	}

	return shaderData;
}

bool EqualsVector(float lhs, float rhs) {
	return fabs(lhs - rhs) <= Epsilon ? true : false;
}