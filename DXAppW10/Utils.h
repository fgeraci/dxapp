#pragma once

#include "pch.h"
#include <fstream>

using namespace std;

vector<byte> LoadShaderFile(std::string pFileName);

/* To check and compensate for floating point error */
bool EqualsVector(float lhs, float rhs);