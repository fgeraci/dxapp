#include "pch.h"

#pragma once

/*	Vector and Points

	points define a location in 3-space while Vectors define magnitude and direction.

	Vectors
	-------

	These have Magnitude and Direction.

	Magnitude(v) --> ||v|| = sqrt(v1^2 + ... + vN^2);

	Standard vectors are those with their tails at origin.
	A vector can be identified by 3 coordinates for its head, v(X,Y,Z)

	Translating a vector, or putting within a different reference frame (coordiante system)
	does NOT changes its magnitude nor direction.

	Operations/Properties:
		Addition, Scalar Multiplication, Substraction, Zero Vector, Negation (flipping)
		All operations are components-wise.
			u + v	= (u1 + v1, ... , uN + vN)
				Applied forces can be represented as the addition of vector to determining the resulting vector force
			u - v	= u + (-1)v	= (u1 + (-1)v1, ... , uN + (-1)vN)
				This is practical to obtain the distance betweeh to vectors (the resulting's vectors magnitude)
				u - v = c --> ||c|| = distance from u to v
			ku		= (ku1, ... , kuN)
			Zero	= (0, ... , 0)
			-v		= (-1)v = ((-1)v1, ... , (-1)*vN)

	* NORMALIZE: 
		Sometimes we do not need to know the magnitud of a vector, for this we can simply
		Normalized(v) = v (1/||v||) = (v1 / ||v||, ... , vN / ||v||)

	* DOT Product:
		DOTP(v,u) = v * u = v1 * u1 + ... + vN * uN --> single scalar value
		Law of cosines: DOTP(u,v) = ||u|| ||v|| cos(t) w/ t as the angle in between v and u, 0
		such that 0 <= t <= PI
		if u dot v = ||v|| ||u|| cos t then u dot v / ||u|| ||v|| = cos t
		hence VERY IMPORTANT: u dot v = cos t IFF u and v are unit vectors (normalized)
			u dot v = 0		--> vectors are orthogonal (perpendicular)
			u dot v > 0		-->	vectors are acute (angle less 90 degrees)
			u dot v < 0		--> vectors are obtuse (angle is greater than 90)

		Finding an angle between u and v
			1)		DOTP(u,v) / ||u|| ||v|| = cos t --> result
			2)		arccos(cos t) = t
			HENCE	arccos(DOTP(u,v) / ||u|| ||v||) = t (degrees)
	* PROJECTIONS and PERPENDICULARS:		
		Derivation:
		1) (0,0) ---> 1 (unit vector n) -------> N (vector p) on X
			p = k*n
		2) Observe k = ||v|| cos(t) << hypothenuse of the traingle formed by v and k*n
		3) if p = kn = (||v|| cot(t)) n = (||v|| dot 1 cot(t)) n = (||v|| ||n|| cot(t)) n
			= (v dot n) n = k n = p
			Remeber n is a unit vector

		v dot n = p <-- PROJ(v on n) = p
		- p is th portion of the force of v acting on direction n
		
		Perpendicular: perp(v on n) = v - p // is the force which acts orthogonal to n 
			Moreover, v = p + w = proj(v on n) + perp(v on n) 

		Summary: for any vector v, it can be orthogonally projected onto u if we
		normalize u first, hence proj(v on u)	= (v dot u / ||u||) u / ||u|| 
												= ((u dot v) / ||u||^2) u
	* ORTHOGONALITY:
		Orthonomal Set - we re-accomodate vectors so they are all perpendicular
			Given a set S of vectors, every vector is an unit vector orthogonal to each other
			Orthogonalize a set is a common set for CG - this is caused by floating errors
			To orthogonalize a set, we would work in pairs of vectors
				Given a set (v0,v1) - 2D:
					1) project v0 onto v1 proj(v0 onto v1)
					2) ORTH(v0 to v1) = v0 - proj(v0 onto v1)
				For 3D set (v0,v1,v2)
					1) Make ORTH(v0 to v1)
					2) Make v2 orthogonal to v0 and v1: v2 - proj(v2 to v0) - proj(v2 to v1)
					3) Normalize all vectors in the set
				General orthogonalization is done via the Gram-Schmidt Orth. Process.
					base case v0 = w0 (w signifis the normalized set)
					then 1 <= i <= n-1 --> vi - SUMj=0,i-1(proj(vi on wj))
					then normalize NORM(wi) = wi / ||wi||
	* CROSS PRODUCT
		Only defined for 3D vectors. The result is another vector orthogonal to u and v

				let w = u x v = (uy*vz-uz*vy,uz*vx-ux*vz,ux*vy-uy*vx)

		Then DOTP(w,u) = DOTP(w,v) = 0 hence obtaining w helps in adding to an orthonormal set.

		For a single 2D vector, should we want to find an orthogonal vector u' to a given vector u,
		we need to :
				let u' = (-uy,ux) then do = DOTP(u,'u) = ux*-uy + uy*ux = 0
				This also holds for DOTP(u , -u') = 0

		Orthogonalization with Cross Product: for this, given a set V {v0,v1,v2}, we can:
			1) normalize v0 = w0
			2) take the CROSSP(w0,v1/NORM(w0,v1)) = w1
			3) take CROSSP(w1,w0) = w2 --> W(w0,w1,w2)
				
	*/

/* THIS IS A SELF DEFINITION OF AN XMVECTOR4D */

struct Vector3 {
	float X, Y, Z;	// 4 bytes each
	float color;	// begins at byte 12 of our struct
};