﻿#pragma once

/* Define this macro for debugging only in _DEBUG */
#ifdef _DEBUG
#define DebugMessage( str ) OutputDebugString( str );
#else
#define DebugMessage( str ) ;
#endif
/* ---------------------------------------------- */

#include <wrl.h>
#include <wrl/client.h>
#include <dxgi1_4.h>
#include <d3d11_3.h>
#include <d2d1_3.h>
#include <d2d1effects_2.h>
#include <dwrite_3.h>
#include <wrl.h>
#include <wrl/client.h>
#include <d3d11_2.h>
#include <d2d1_2.h>
#include <d2d1effects_1.h>
#include <dwrite_2.h>
#include <wincodec.h>
#include <DirectXColors.h>

// replaces xnamath.h
#include <DirectXMath.h>
#include <memory>
#include <agile.h>
#include <concrt.h>
#include <collection.h>
#include <ppltasks.h>

/* Added */
// base DR2D
#include <d2d1.h>
#include <d2d1_1.h>

/* My classes */
#include "Vector3.h"
#include "Hud.h"
#include "Game.h"